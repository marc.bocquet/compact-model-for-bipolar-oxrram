# Compact Model for bipolar OxRRAM

This project proposes a physics-based compact model used in electrical simulator for bipolar OxRAM memories.

This is the model implemented in the pulbications: 
* https://doi.org/10.1109/TED.2013.2296793
* https://doi.org/10.1109/ACCESS.2020.3000867

**If you use this model, please provide these two bibliographic references.**

## File description
* OxRAM.vla: OxRAM cell model description file in the VerilogA language
* oxram_TiO2_UNICO.lib: Model card of TiO2-based OxRAM devices manufactured by 3IT (Université de Sherbrooke) - v2020
* oxram_HfO2_IEEEAccess2020.lib: Model card of HfO2-based OxRAM published in this article: https://doi.org/10.1109/ACCESS.2020.3000867
* test_model.cir: test circuit
* LICENSE: GNU General Public License v3.0

## Usage

This model is designed to be associated in series with an element (resistor, transistor) limiting the current during the FORMING and SET operations. The test_model.cir does not contain this element: it is up to the user of the model to add it.

## Support/Guarantee
No guarantee is provided that the results produced by this model are true. 
The differences with reality can be multiple (bad use of the model, not too large simulation, model card not corresponding to the manufactured memories, model used outside its area of validity, etc...). 
No support is provided

## Authors
Marc Bocquet, full professor at the IM2NP laboratory of Aix-Marseille University.

## License
GNU General Public License v3.0

